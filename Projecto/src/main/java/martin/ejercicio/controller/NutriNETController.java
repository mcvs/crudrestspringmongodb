package martin.ejercicio.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import martin.ejercicio.model.Cliente;
import martin.ejercicio.repository.ClienteRepository;
import martin.ejercicio.service.SequenceGeneratorService;

@RestController()
@RequestMapping("/NutriNET")
public class NutriNETController {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private SequenceGeneratorService sGService;

	@PostMapping("/Cliente")
	public Map<String, Object>  agregarCliente(@RequestBody Cliente cliente) {
		Map<String, Object> respuesta = new HashMap<>();
		if (clienteRepository.findByNombreUsuario(cliente.getNombreUsuario()).size() <= 0 && clienteRepository.findByCorreoElectronico(cliente.getCorreoElectronico()).size() <= 0 ) {
			cliente.setId(sGService.generateSequence(Cliente.SEQUENCE_NAME));
			clienteRepository.save(cliente);
			respuesta.put("cve_error", 0);
			respuesta.put("cve_mensaje", "Cliente creado exitosamente");
			respuesta.put("cliente", cliente);
		} else {
			respuesta.put("cve_error", -1);
			respuesta.put("cve_mensaje", "El nombre o correo electronico del clientes ya se encuentran registrados"); 
		
		}

		return respuesta;
	}

	@GetMapping("/Cliente/{id}")
	public Map<String, Object> getCliente(@PathVariable Long id) {
		Map<String, Object> respuesta = new HashMap<>();
		Optional<Cliente> c = clienteRepository.findById(id);
		if (c.isPresent()) {
			respuesta.put("cliente", c);
			respuesta.put("cve_error", 0);
			respuesta.put("cve_mensaje", "Cliente encontrado"); // status code 200 ok

		} else {
			respuesta.put("cve_error", -1);
			respuesta.put("cve_mensaje", "No se encuentra el cliente"); // status code 404 no found
		}
		return respuesta;
	}

	@GetMapping("/Cliente")
	public List<Cliente> getClienteTodos() {
		return clienteRepository.findAll();

	}

	@PutMapping("/Cliente/{id}")
	public Map<String, Object> editarCliente(@PathVariable Long id, @RequestBody Cliente cUpdate) {
		Map<String, Object> respuesta = new HashMap<>();
		Optional<Cliente> c = clienteRepository.findById(id);
		if (c.isPresent()) {
			Cliente c2 = c.get();
			c2.setEdad(cUpdate.getEdad());
			c2.setEstatura(cUpdate.getEstatura());
			final Cliente clienteActualizado = clienteRepository.save(c2);
			respuesta.put("cliente", clienteActualizado);
			respuesta.put("cve_error", 0);
			respuesta.put("cve_mensaje", "Cliente actualizado"); // status code 200 ok
		} else {
			respuesta.put("cve_error", -1);
			respuesta.put("cve_mensaje", "No se encuentra cliente"); // status code 404 no found
		}

		return respuesta;
	}
}
