package martin.ejercicio.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mongodb.lang.NonNull;
	

@Document(collection = "cliente")
public class Cliente {
	
	
	@Transient
	public static final String SEQUENCE_NAME = "users_sequence";
	@Id
	private Long id;
	
	private int clienteID;
	private String nombreUsuario;
	private String contrasena;
	private String nombre;
	private String apellidos;

	private String correoElectronico;
	private int edad;
	private double estatura;
	private double peso;
	private double imc;
	private double geb;
	private double eta;
	private Date fechaCreacion;
	private Date fechaActualizacion;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getClienteID() {
		return clienteID;
	}
	public void setClienteID(int clienteID) {
		this.clienteID = clienteID;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	@NonNull
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public double getEstatura() {
		return estatura;
	}
	public void setEstatura(double estatura) {
		this.estatura = estatura;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getImc() {
		return imc;
	}
	public void setImc(double imc) {
		this.imc = imc;
	}
	public double getGeb() {
		return geb;
	}
	public void setGeb(double geb) {
		this.geb = geb;
	}
	public double getEta() {
		return eta;
	}
	public void setEta(double eta) {
		this.eta = eta;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	@Override
	public String toString() {
		return "Cliente [id=" + id + ", clienteID=" + clienteID + ", nombreUsuario=" + nombreUsuario + ", contrasena="
				+ contrasena + ", nombre=" + nombre + ", apellidos=" + apellidos + ", correoElectronico="
				+ correoElectronico + ", edad=" + edad + ", estatura=" + estatura + ", peso=" + peso + ", imc=" + imc
				+ ", geb=" + geb + ", eta=" + eta + ", fechaCreacion=" + fechaCreacion + ", fechaActualizacion="
				+ fechaActualizacion + "]";
	}
	
	public Cliente(Long id, int clienteID, String nombreUsuario, String contrasena, String nombre, String apellidos,
			String correoElectronico, int edad, double estatura, double peso, double imc, double geb, double eta,
			Date fechaCreacion, Date fechaActualizacion) {
		super();
		this.id = id;
		this.clienteID = clienteID;
		this.nombreUsuario = nombreUsuario;
		this.contrasena = contrasena;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.correoElectronico = correoElectronico;
		this.edad = edad;
		this.estatura = estatura;
		this.peso = peso;
		this.imc = imc;
		this.geb = geb;
		this.eta = eta;
		this.fechaCreacion = fechaCreacion;
		this.fechaActualizacion = fechaActualizacion;
	}
	public Cliente() {
		
	}
	
	
	
	
	
	
}
