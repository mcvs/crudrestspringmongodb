package martin.ejercicio.repository;



import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import martin.ejercicio.model.Cliente;
@Repository
public interface ClienteRepository extends MongoRepository<Cliente, Long>{
	List<Cliente> findByNombre(String nombre);
	List<Cliente> findByNombreUsuario(String username);
	List<Cliente> findByCorreoElectronico(String correo);
}
