
import java.util.Random;
import java.util.Scanner;

public class Principal {
	public static void main(String... args) {
		Persona p = new Persona();
		Scanner sc = new Scanner(System.in);
		System.out.println("* Introduzca nombre: ");
		p.setNombre(sc.nextLine());
		System.out.println("* Introduzca tu/tus apellidos: ");
		p.setApellidos(sc.nextLine());
		System.out.println("* Introduzca la edad");
		p.setEdad(sc.nextInt());
		if(p.esMayorDeEdad()) {
			System.out.println("------Eres mayor de edad------");
		}else {
			System.out.println("------No eres mayor de edad------");
		}
		
		System.out.println("*Introduzca el genero: ");
		p.setSexo('M');
		//System.out.println(p.comprobarSexo());
		System.out.println("* Introduzca su peso: ");
		p.setPeso(sc.nextDouble());
		System.out.println("* Introduzca su altura en metros: ");
		p.setAltura(sc.nextDouble());
		System.out.println("--Peso debajo del ideal(-1), Peso ideal(0), Sobrepeso(1) --");
		System.out.println(p.calcularIMC());
		System.out.println(p);
		
	}

}

class Persona {
	// Las propiedas de clase tiene valores por defectos
	private String nombre;
	private String apellidos;
	private int edad;
	private String nss;
	private char sexo;
	private double peso;
	private double altura;

	public Persona() {
		this.nss = generaNSS();
	}

	public Persona(String nombre, String apellidos, int edad, String nss, char sexo, double peso, double altura) {
		
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.nss = generaNSS();
		this.sexo = sexo;
		this.peso = peso;
		this.altura = altura;
	}

	/*
	 * Calcula el IMC de una persona Formula (peso en kg/(Estatura^2 en m)) Devuelve
	 * -1 si esta por debajo de su peso ideal Devuelve 0 si esta en su peso ideal.
	 * Devuelve 1 si esta en sobrepreso
	 */
	public int calcularIMC() {
		final int DEBAJO_PESO_IDEAL = -1;
		final int PESO_IDEAL = 0;
		final int SOBREPESO = 1;
		int pesoIdeal;

		double imc = this.peso / Math.pow(this.altura, 2);
		//double res = this.peso / (this.altura * this.altura);
		
		if (this.sexo == 'H') {
			if (imc < 20) {
				return DEBAJO_PESO_IDEAL;
			} else if (imc >= 20 && imc < 25) {
				return PESO_IDEAL;
			}else{
				return SOBREPESO;
			}
		} else if (this.sexo == 'M') {
			if (imc < 19) {
				return DEBAJO_PESO_IDEAL;
			} else if (imc >= 19 && imc < 24) {
				return PESO_IDEAL;
			} else {
				return SOBREPESO;
			}
		}else{
			return -2;
		}
		
	}

	/**
	 * 
	 * @return true si es mayor de edad
	 */
	public boolean esMayorDeEdad() {
		return this.edad >= 18;

	}

	private boolean comprobarSexo() {
		return (this.sexo != 'H') ? true : false;
	}
//cambiar el valor a publico ****************************************************
	private String generaNSS() {
		final String caracteres = "0123456789ABCDE";
		final int longitudChar = caracteres.length();
		String nssRandom = "";

		Random r = new Random();

		for (int i = 0; i < 8; i++) {
			nssRandom += (caracteres.charAt(r.nextInt(longitudChar)));
		}
		return nssRandom;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	
	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellidos=" + apellidos + ", edad=" + edad + ", nss=" + generaNSS() + ", sexo="
				+ sexo + ", peso=" + peso + ", altura=" + altura + "]";
	}

}